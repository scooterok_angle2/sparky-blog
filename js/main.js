let appInit = function(){    
    document.querySelector('#loader').classList.add('done');
    setTimeout(function(){
        document.querySelector('#loader').remove();
    }, 1000);
    var scrollbar = Scrollbar.init(document.querySelector('#scrollbar'), {        
        damping: 0.08        
    });
    if(document.body.classList.contains('article')){
        scrollbar.addListener(function(status){
            var h = window.innerHeight - 80;
            if(status.offset.y > h){
                document.querySelector('.header__logo .back').classList.add('black');
            }else{
                document.querySelector('.header__logo .back').classList.remove('black');
            }        
        });        
    }
    if(document.body.classList.contains('list')){
        document.querySelector('.header__social_button').addEventListener('click', function(e){
            document.querySelector('.header__social').classList.toggle('active');
        });
        $('.list__tabs > li').click(function(e){
            if($(e.currentTarget).hasClass('current'))return;
            let id = $(e.currentTarget).attr('id');
            $('.list__tabs li.current').removeClass('current');
            $(e.currentTarget).addClass('current');
            $('.list__catalog.current').removeClass('current')
            $('.list__catalog[data-id="'+id+'"]').addClass('current');
            console.log($('.list__catalog[data-id="'+id+'"]'));
            
        });
    }    
    let share = document.querySelectorAll('#share-instagram, #share-facebook, #share-twitter');
    if(share.length){
        let link = document.location.href;
        let title = document.querySelector('h1').innerText;
        let params = {
            instagram : 'https://www.linkedin.com/shareArticle?mini=true&url='+link+'&title='+title+'&source=LinkedIn',
            facebook : 'https://www.facebook.com/sharer/sharer.php?u='+link,
            twitter : 'https://twitter.com/intent/tweet?text='+title+'&url='+link
        }        
        share.forEach(function(el, i){            
            var id = el.id.replace('share-', '');            
            document.querySelector('#share-'+id).href = params[id];
        });
    }
    
}
window.onload = appInit;